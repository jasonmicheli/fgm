library(tidyquant)
library(plotly)

#### begin parameters ####
# set timeframe for plot zoomed-in view
plot_zoom_from <- today() - months(10)
plot_zoom_to <- today()

# set timeframe for performance reports
performance_from <- "1992-01-01"
performance_to <- today()

# set initial portfolio value
starting_value <- 10000

# set number of days lag between signal and execution of trade
execution_lag <- 1

# set the annual advisory fee
advisor_fee <- .02

# set the ticker for baseline
baseline <- "^GSPC"
#### end parameters ####

#### begin filters ####
# set number of days to use for moving average
day_hy <- 19
day_munihy <- 19
# set number of days to use for rsi
day_rsi_hy <- 14
day_rsi_munihy <- 14
# set number of trading days to have the stop-loss in effect
day_stoploss_hy <- 20
day_stoploss_muni <- 20
# set stop loss threshold
stop_loss_threshold_hy <- .005
stop_loss_threshold_muni <- .005
# set buy-signal threshold above the moving average
threshold_hy <- .005
threshold_munihy <- .005
# set wait period in business days between a sell and the next buy
wait <- 10
#### end filters ####

#### begin data import ####
# import lipper hy index data
lipper_hy <- read_csv("data_inputs/lipper_hy.csv",
                      col_types = cols(
                              date = col_date(format = "%m/%d/%y"),
                              value = col_double()
                      ))
head(lipper_hy)
tail(lipper_hy)

# import lipper muni hy data
lipper_muni <- read_csv("data_inputs/optax.csv",
                        col_types = cols(
                                date = col_date(format = "%m/%d/%y"),
                                value = col_double()
                        ))
head(lipper_muni)
tail(lipper_muni)
#### end data import ####

#### begin ema plots ####
# add in EMA for lipper hy
lipper_hy <- lipper_hy %>%
        tq_mutate(select = value, mutate_fun = EMA, n=day_hy)

# add in EMA for lipper muni
lipper_muni <- lipper_muni %>%
        tq_mutate(select = value, mutate_fun = EMA, n=day_munihy)

# plot lipper hy with ema
plot_ly() %>%
        add_trace(data = lipper_hy,
                  x = ~date,
                  y = ~value,
                  type = "scatter",
                  name = 'Lipper HY',
                  mode = 'lines',
                  line = list(color = "#00526d")) %>%
        add_trace(data = lipper_hy,
                  x = ~date,
                  y = ~EMA,
                  type = "scatter",
                  name = 'EMA',
                  mode = 'lines',
                  line = list(color = "#de6e6e")) %>% 
        layout(xaxis = list(title = "", 
                            range = c(plot_zoom_from, plot_zoom_to)
                            ),
               yaxis = list(title = "", 
                            range = c(2000,2300)
                            )
               )

## plotly lipper muni with ema
plot_ly() %>%
        add_trace(data = lipper_muni,
                  x = ~date,
                  y = ~value,
                  type = "scatter",
                  name = 'Lipper Muni',
                  mode = 'lines',
                  line = list(color = "#00526d")) %>%
        add_trace(data = lipper_muni,
                  x = ~date,
                  y = ~EMA,
                  type = "scatter",
                  name = 'EMA',
                  mode = 'lines',
                  line = list(color = "#de6e6e")) %>% 
        layout(xaxis = list(title = "", range = c(plot_zoom_from, plot_zoom_to))) %>% 
        layout(yaxis = list(title = "", range = c(6.7,7.7)))
#### end ema plots ####

#### begin trading signal calculations ####
# calculate the rsi for lipper hy
lipper_hy <- lipper_hy %>% 
        tq_mutate(select = value,
                  mutate_fun = RSI,
                  n = day_rsi_hy)

# calculate the rsi for lipper muni
lipper_muni <- lipper_muni %>% 
        tq_mutate(select = value,
                  mutate_fun = RSI,
                  n = day_rsi_munihy)

# calculate the difference between the EMA and lipper hy index value
lipper_hy <- lipper_hy %>%
        mutate(difference_value_and_ema = value - EMA)

# calculate the difference between the EMA and lipper muni index value
lipper_muni <- lipper_muni %>%
        mutate(difference_value_and_ema = value - EMA)

# create binary indicator (1=yes, 0=no) for when lipper hy index (value) is greater than 
# moving average (EMA)
lipper_hy <- 
        lipper_hy %>%
        mutate(
                value_greater_than_ema = ifelse(value > EMA, 1, 0)
        )

# create binary indicator (1=yes, 0=no) for when lipper muni index (value) is greater than 
# moving average (EMA)
lipper_muni <- 
        lipper_muni %>%
        mutate(
                value_greater_than_ema = ifelse(value > EMA, 1, 0)
        )

# create set variable for periods when lipper hy value is above/below ema
resolution <- 0
idx <- c(resolution, diff(lipper_hy$value_greater_than_ema))
i2 <- c(1,which(idx != resolution), nrow(lipper_hy)+1)
lipper_hy$set <- rep(1:length(diff(i2)), diff(i2))

# create set variable for periods when lipper muni value is above/below ema
resolution3 <- 0
idx5 <- c(resolution3, diff(lipper_muni$value_greater_than_ema))
idx6 <- c(1,which(idx5 != resolution3), nrow(lipper_muni)+1)
lipper_muni$set <- rep(1:length(diff(idx6)), diff(idx6))

# add sell threshold with stop-loss included for lipper hy
lipper_hy <- lipper_hy %>%
        arrange(date) %>%
        group_by(set) %>%
        mutate(trading_days_in_set = row_number()
               ) %>%
        ungroup() %>%
        mutate(lagging_trading_day = lag(trading_days_in_set, 1)
               ) %>%
        group_by(set) %>%
        mutate(sell_threshold = ifelse(value_greater_than_ema == 0, EMA,
                                       ifelse(lagging_trading_day < day_stoploss_hy,
                                              (1-stop_loss_threshold_hy)*head(EMA, 1),
                                              EMA
                                              )
                                       )
                                       
               ) %>%
        ungroup()

# add sell threshold with stop-loss included for lipper muni
lipper_muni <- lipper_muni %>%
        arrange(date) %>%
        group_by(set) %>%
        mutate(trading_days_in_set = row_number()
               ) %>%
        ungroup() %>%
        mutate(lagging_trading_day = lag(trading_days_in_set, 1)
               ) %>%
        group_by(set) %>%
        mutate(sell_threshold = ifelse(value_greater_than_ema == 0, EMA,
                                       ifelse(lagging_trading_day < day_stoploss_hy,
                                              (1-stop_loss_threshold_hy)*head(EMA, 1),
                                              EMA
                                              )
                                       )
               ) %>%
        ungroup()

# group by set, arrange by date, filter for value > EMA for each set calculates
# what the threshold value is (calculated from first value for that set) and
# calculates whether value is greater than the threshold
lipper_hy <- lipper_hy %>%
        group_by(set) %>%
        arrange(date) %>%
        mutate(buy_threshold = (1+threshold_hy)*head(EMA, 1),
               value_is_greater_than_buy_threshold = ifelse(value_greater_than_ema == 1, 
                                                            ifelse(value > buy_threshold,
                                                                   1,
                                                                   0),
                                                            ifelse(value > sell_threshold,
                                                                   0,
                                                                   -999)
                                                            )
               ) %>%
        ungroup()

# group by set, arrange by date, filter for value > EMA for each set calculates
# what the threshold value is (calculated from first value for that set) and
# calculates whether value is greater than the threshold
lipper_muni <- lipper_muni %>%
        group_by(set) %>%
        arrange(date) %>%
        mutate(buy_threshold = (1+threshold_munihy)*head(EMA, 1),
               value_is_greater_than_buy_threshold = ifelse(value_greater_than_ema == 1, 
                                                            ifelse(value > buy_threshold,
                                                                   1,
                                                                   0),
                                                            ifelse(value > sell_threshold,
                                                                   0,
                                                                   -999)
                                                            )
               ) %>%
        ungroup()

# create lipper hy buy/sell signals: 1 = buy, 0 = hold, -999 = sell
# difference in value_is_greater_than_buy_threshold column: 0 is do nothing, 
# -1 is do nothing, -999 is sell, +999 do nothing, +1 buy, -1000 sell, +1000 buy
idx3 <- c(-999,diff(lipper_hy$value_is_greater_than_buy_threshold))
lipper_hy <- lipper_hy %>%
        mutate(change_ema_threshold = idx3)
lipper_hy <- lipper_hy %>%
        mutate(signals = ifelse(lipper_hy$change_ema_threshold == -1,
                                            0,
                                            ifelse(lipper_hy$change_ema_threshold == 0,
                                                   0,
                                                   ifelse(lipper_hy$change_ema_threshold == -999,
                                                          -999,
                                                          ifelse(lipper_hy$change_ema_threshold == 999,
                                                                 0,
                                                                 ifelse(lipper_hy$change_ema_threshold == 1,
                                                                        1,
                                                                        ifelse(lipper_hy$change_ema_threshold == -1000,
                                                                               -999,
                                                                               ifelse(lipper_hy$change_ema_threshold == 1000,
                                                                                      1,
                                                                                      88))))))))

# create buy/sell signals: 1 = buy, 0 = hold, -999 = sell
# difference in value_is_greater_than_buy_threshold column: 0 is do nothing, 
# -1 is do nothing, -999 is sell, +999 do nothing, +1 buy, -1000 sell, +1000 buy
idx7 <- c(-999,diff(lipper_muni$value_is_greater_than_buy_threshold))
lipper_muni <- lipper_muni %>%
        mutate(change_ema_threshold = idx7)
lipper_muni <- lipper_muni %>%
        mutate(signals = ifelse(lipper_muni$change_ema_threshold == -1,
                                0,
                                ifelse(lipper_muni$change_ema_threshold == 0,
                                       0,
                                       ifelse(lipper_muni$change_ema_threshold == -999,
                                              -999,
                                              ifelse(lipper_muni$change_ema_threshold == 999,
                                                     0,
                                                     ifelse(lipper_muni$change_ema_threshold == 1,
                                                            1,
                                                            ifelse(lipper_muni$change_ema_threshold == -1000,
                                                                   -999,
                                                                   ifelse(lipper_muni$change_ema_threshold == 1000,
                                                                          1,
                                                                          88))))))))
#### end trading signal calculations ####

#### begin DEBUG errors in the signals (result should be 0) ####
which(lipper_hy$signals == 88)
which(lipper_muni$signals == 88)
#### end DEBUG errors in the signals (result should be 0) ####

#### begin portfolio holdings calcuations ####
# insert lag for time between trade signal and trade execution
lipper_hy <- lipper_hy %>%
        mutate(trade_day = lag(lipper_hy$signals, n=execution_lag))

# insert lag for time between trade signal and trade execution
lipper_muni <- lipper_muni %>%
        mutate(trade_day = lag(lipper_muni$signals, n=execution_lag))

# create set variable for periods by buy/sell lipper hy
resolution2 <- 0
idx4 <- c(1,which(lipper_hy$trade_day != resolution2), nrow(lipper_hy)+1)
lipper_hy$set2 <- rep(1:length(diff(idx4)), diff(idx4))

# create set variable for periods by buy/sell lipper muni
resolution4 <- 0
idx8 <- c(1,which(lipper_muni$trade_day != resolution4), nrow(lipper_muni)+1)
lipper_muni$set2 <- rep(1:length(diff(idx8)), diff(idx8))

# group by set, arrange by date, filter for in_hy_bonds (1) vs out_of_hy_bonds (-999)
lipper_hy <- lipper_hy %>%
        group_by(set2) %>%
        arrange(date) %>%
        mutate(in_or_out_hy_bonds = head(trade_day, 1)
               ) %>%
        ungroup()

# group by set, arrange by date, filter for in_hy_munis (1) vs out_of_hy_munis (-999)
lipper_muni <- lipper_muni %>%
        group_by(set2) %>%
        arrange(date) %>%
        mutate(in_or_out_hy_munis = head(trade_day, 1)
        ) %>%
        ungroup()

# create initial holdings of lipper hy program
holdings_hy_program_hy <- c()
holdings_hy_program_hy[1:(day_hy+1)] <-0
holdings_hy_program_cash <-c()
holdings_hy_program_cash[1:(day_hy+1)] <- starting_value  

# create initial holdings of lipper muni program
holdings_muni_program_munis <- c()
holdings_muni_program_munis[1:(day_munihy+1)] <-0
holdings_muni_program_cash <-c()
holdings_muni_program_cash[1:(day_munihy+1)] <- starting_value

# create initial holdings of tactical program
holdings_tactical_program_hy <- c()
holdings_tactical_program_hy[1:(day_hy+1)] <-0
holdings_tactical_program_muni <- c()
holdings_tactical_program_muni[1:(day_hy+1)] <-0
holdings_tactical_program_cash <-c()
holdings_tactical_program_cash[1:(day_hy+1)] <- starting_value 

# calculate the holdings of each day with hy_bonds in shares and cash in dollars
price_hy_program_hy <- lipper_hy$value
trade_hy_program_hy <- Lag(lipper_hy$signals, execution_lag)
trade_hy_program_hy[is.na(trade_hy_program_hy)] <- 0
trade_hy_program_hy[trade_hy_program_hy == -999] <- -1
for (i in (day_hy+1): length(price_hy_program_hy)){
        if (trade_hy_program_hy[i]>=0){
                holdings_hy_program_hy[i] <- holdings_hy_program_hy[i-1] + holdings_hy_program_cash[i-1]/price_hy_program_hy[i-(execution_lag-1)]*trade_hy_program_hy[i]
                holdings_hy_program_cash[i] <- holdings_hy_program_cash[i-1] - 
                        holdings_hy_program_cash[i-1]/price_hy_program_hy[i-(execution_lag-1)]*trade_hy_program_hy[i]*price_hy_program_hy[i-(execution_lag-1)]
        } else{
                holdings_hy_program_hy[i] <- 0
                holdings_hy_program_cash[i] <- holdings_hy_program_cash[i-1] + 
                        holdings_hy_program_hy[i-1]*price_hy_program_hy[i-(execution_lag-1)]
                }
        }
holdings_hy_program_hy<-reclass(holdings_hy_program_hy,price_hy_program_hy)
holdings_hy_program_cash<-reclass(holdings_hy_program_cash,price_hy_program_hy)
lipper_hy <- lipper_hy %>% 
        mutate(hy_bond_holdings_in_shares = holdings_hy_program_hy) %>% 
        mutate(cash_holdings_in_dollars = holdings_hy_program_cash)

# calculate the holdings of each day with holdings_muni_program_munis in shares and cash in dollars
price_muni_program_muni <- lipper_muni$value
trade_muni_program_muni <- Lag(lipper_muni$signals, execution_lag)
trade_muni_program_muni[is.na(trade_muni_program_muni)] <- 0
trade_muni_program_muni[trade_muni_program_muni == -999] <- -1
for (i in (day_munihy+1): length(price_muni_program_muni)){
        if (trade_muni_program_muni[i]>=0){
                holdings_muni_program_munis[i] <- holdings_muni_program_munis[i-1] + holdings_muni_program_cash[i-1]/price_muni_program_muni[i-(execution_lag-1)]*trade_muni_program_muni[i]
                holdings_muni_program_cash[i] <- holdings_muni_program_cash[i-1] - 
                        holdings_muni_program_cash[i-1]/price_muni_program_muni[i-(execution_lag-1)]*trade_muni_program_muni[i]*price_muni_program_muni[i-(execution_lag-1)]
        } else{
                holdings_muni_program_munis[i] <- 0
                holdings_muni_program_cash[i] <- holdings_muni_program_cash[i-1] + 
                        holdings_muni_program_munis[i-1]*price_muni_program_muni[i-(execution_lag-1)]
        }
        }
holdings_muni_program_munis<-reclass(holdings_muni_program_munis,price_muni_program_muni)
holdings_muni_program_cash<-reclass(holdings_muni_program_cash,price_muni_program_muni)
lipper_muni <- lipper_muni %>% 
        mutate(hy_muni_holdings_in_shares = holdings_muni_program_munis) %>% 
        mutate(cash_holdings_in_dollars = holdings_muni_program_cash)

# calculate the holdings of each day for tactical program with bonds in shares and cash in dollars
x <- lipper_hy %>% 
        select(date, signals, in_or_out_hy_bonds)
y <- lipper_muni %>% 
        select(date, signals, in_or_out_hy_munis)
colnames(x) <- c("date", "signals_hy", "in_or_out_hy_bonds")
colnames(y) <- c("date", "signals_muni", "in_or_out_hy_munis")
tactical_program <- left_join(x, y, by = "date")
rm(x)
rm(y)
price_tactical_program_hy <- lipper_hy$value
price_tactical_program_muni <- lipper_muni$value
trade_tactical_program_hy <- Lag(tactical_program$signals_hy, execution_lag)
trade_tactical_program_hy[is.na(trade_tactical_program_hy)] <- 0
trade_tactical_program_hy[trade_tactical_program_hy == -999] <- -1
trade_tactical_program_muni <- Lag(tactical_program$signals_muni, execution_lag)
trade_tactical_program_muni[is.na(trade_tactical_program_muni)] <- 0
trade_tactical_program_muni[trade_tactical_program_muni == -999] <- -1
in_tactical_program_muni <- tactical_program$in_or_out_hy_munis
in_tactical_program_muni[is.na(in_tactical_program_muni)] <- -999
in_tactical_program_hy <- tactical_program$in_or_out_hy_bonds
in_tactical_program_hy[is.na(in_tactical_program_hy)] <- -999
for (i in (day_hy+1): length(price_tactical_program_hy)){
        if (trade_tactical_program_hy[i]>0){
                holdings_tactical_program_hy[i] <- holdings_tactical_program_cash[i-1]/price_tactical_program_hy[i] + holdings_tactical_program_muni[i-1]*price_tactical_program_muni[i]/price_tactical_program_hy[i] + holdings_tactical_program_hy[i-1]
                holdings_tactical_program_muni[i] <- 0
                holdings_tactical_program_cash[i] <- 0
        } else if (trade_tactical_program_hy[i]<0){
                if (in_tactical_program_hy[i-1]==1){
                        if (in_tactical_program_muni[i]==1){
                                holdings_tactical_program_muni[i] <- holdings_tactical_program_hy[i-1]*price_tactical_program_hy[i]/price_tactical_program_muni[i]
                                holdings_tactical_program_hy[i] <- 0
                                holdings_tactical_program_cash[i] <- 0
                        } else {
                                holdings_tactical_program_muni[i] <- 0
                                holdings_tactical_program_hy[i] <- 0
                                holdings_tactical_program_cash[i] <- holdings_tactical_program_hy[i-1]*price_tactical_program_hy[i]
                        }
                } else {
                        if (in_tactical_program_muni[i-1]==1){
                                if (trade_tactical_program_muni[i]==-1){
                                        holdings_tactical_program_muni[i] <- 0
                                        holdings_tactical_program_hy[i] <- 0
                                        holdings_tactical_program_cash[i] <- holdings_tactical_program_muni[i-1]*price_tactical_program_muni[i]
                                } else {
                                        holdings_tactical_program_muni[i] <- holdings_tactical_program_muni[i-1]
                                        holdings_tactical_program_hy[i] <- holdings_tactical_program_hy[i-1]
                                        holdings_tactical_program_cash[i] <- holdings_tactical_program_cash[i-1]
                                }
                        } else {
                                if (trade_tactical_program_muni[i]==1){
                                        holdings_tactical_program_muni[i] <- holdings_tactical_program_cash[i-1]/price_tactical_program_muni[i]
                                        holdings_tactical_program_hy[i] <- 0
                                        holdings_tactical_program_cash[i] <- 0
                                } else {
                                        holdings_tactical_program_muni[i] <- holdings_tactical_program_muni[i-1]
                                        holdings_tactical_program_hy[i] <- holdings_tactical_program_hy[i-1]
                                        holdings_tactical_program_cash[i] <- holdings_tactical_program_cash[i-1]
                                }
                        }
                }
        } else {
                if (in_tactical_program_hy[i-1]==1){
                        holdings_tactical_program_hy[i] <- holdings_tactical_program_hy[i-1]
                        holdings_tactical_program_cash[i] <- 0
                        holdings_tactical_program_muni[i] <- 0
                } else {
                        if (in_tactical_program_muni[i-1]==1){
                                if (trade_tactical_program_muni[i]==-1){
                                        holdings_tactical_program_hy[i] <- 0
                                        holdings_tactical_program_cash[i] <- holdings_tactical_program_muni[i-1]*price_tactical_program_muni[i] + holdings_tactical_program_cash[i-1]
                                        holdings_tactical_program_muni[i] <- 0
                                } else {
                                        holdings_tactical_program_hy[i] <- 0
                                        holdings_tactical_program_cash[i] <- holdings_tactical_program_cash[i-1]
                                        holdings_tactical_program_muni[i] <- holdings_tactical_program_muni[i-1] 
                                }
                        } else {
                                if (trade_tactical_program_muni[i]==1){
                                        holdings_tactical_program_hy[i] <- 0
                                        holdings_tactical_program_cash[i] <- 0
                                        holdings_tactical_program_muni[i] <- holdings_tactical_program_cash[i-1]/price_tactical_program_muni[i]
                                } else {
                                        holdings_tactical_program_hy[i] <- 0
                                        holdings_tactical_program_cash[i] <- holdings_tactical_program_cash[i-1]
                                        holdings_tactical_program_muni[i] <- holdings_tactical_program_muni[i-1]
                                        }
                                }
                        } 
                }
        }
holdings_tactical_program_hy<-reclass(holdings_tactical_program_hy,price_tactical_program_hy)
holdings_tactical_program_muni<-reclass(holdings_tactical_program_muni,price_tactical_program_hy)
holdings_tactical_program_cash<-reclass(holdings_tactical_program_cash,price_tactical_program_hy)
tactical_program <- tactical_program %>% 
        mutate(tactical_holdings_tactical_program_hy = holdings_tactical_program_hy) %>%
        mutate(tactical_holdings_tactical_program_muni = holdings_tactical_program_muni) %>% 
        mutate(tactical_holdings_tactical_program_cash = holdings_tactical_program_cash)

# calculate daily portfolio equity and daily portfolio return for lipper hy program
equity_hy_program <-c()
equity_hy_program[1:(day_hy+1)] <- starting_value 
return<-c()                  
return[1:(day_hy+1)] <- 0
for (i in (day_hy+1): length(price_hy_program_hy)){
        equity_hy_program[i] <- holdings_hy_program_hy[i] * price_hy_program_hy[i] + holdings_hy_program_cash[i]
        return[i] <- equity_hy_program[i]/equity_hy_program[i-1]-1
        }
equity_hy_program<-reclass(equity_hy_program,price_hy_program_hy)
return<-reclass(return,price_hy_program_hy)
lipper_hy <- lipper_hy %>% 
        mutate(portfolio_equity = equity_hy_program) %>% 
        mutate(portfolio_return = return)

# calculate daily portfolio equity and daily portfolio return lipper muni hy program
equity_muni_program <-c()
equity_muni_program[1:(day_munihy+1)] <- starting_value 
return_muni<-c()                  
return_muni[1:(day_munihy+1)] <- 0
for (i in (day_munihy+1): length(price_muni_program_muni)){
        equity_muni_program[i] <- holdings_muni_program_munis[i] * price_muni_program_muni[i] + holdings_muni_program_cash[i]
        return_muni[i] <- equity_muni_program[i]/equity_muni_program[i-1]-1
        }
equity_muni_program<-reclass(equity_muni_program,price_muni_program_muni)
return_muni<-reclass(return_muni,price_muni_program_muni)
lipper_muni <- lipper_muni %>% 
        mutate(portfolio_equity = equity_muni_program) %>% 
        mutate(portfolio_return = return_muni)

# calculate daily portfolio equity and daily portfolio return for lipper hy
tactical_program_equity <-c()
tactical_program_equity[1:(day_hy+1)] <- starting_value 
tactical_program_return<-c()                  
tactical_program_return[1:(day_hy+1)] <- 0
for (i in (day_hy+1): length(price_tactical_program_hy)){
        tactical_program_equity[i] <- holdings_tactical_program_hy[i]*price_tactical_program_hy[i] + holdings_tactical_program_muni[i]*price_tactical_program_muni[i] + holdings_tactical_program_cash[i]
        tactical_program_return[i] <- tactical_program_equity[i]/tactical_program_equity[i-1]-1
        }
tactical_program_equity<-reclass(tactical_program_equity,price_tactical_program_hy)
tactical_program_return<-reclass(tactical_program_return,price_tactical_program_muni)
tactical_program <- tactical_program %>% 
        mutate(portfolio_equity = tactical_program_equity) %>% 
        mutate(portfolio_return = tactical_program_return)
colnames(tactical_program) <- c("date", "signals_hy", "in_or_out_hy_bonds", "signals_muni", "in_or_out_hy_munis", "tactical_holdings_tactical_program_hycorp", "tactical_holdings_tactical_program_hymuni", "tactical_holdings_tactical_program_cash", "tactical_portfolio_equity", "tactical_portfolio_return")
#### end portfolio holdings calcuations ####

#### begin DEBUG for missing data ####
# check for missing data
colSums(is.na(lipper_hy))
colSums(is.na(lipper_muni))
#### end DEBUG for missing data ####

#### begin plotly of growth of portfolios ####
# plotly growth of lipper hy program portfolio
plot_ly() %>%
        add_trace(data = lipper_hy,
                  x = ~date,
                  y = ~portfolio_equity,
                  type = "scatter",
                  name = 'Portfolio Value',
                  mode = 'lines') %>% 
        layout(yaxis = list(title = "Value of Portfolio"),
               xaxis = list(title = "Date"),
               title = list(text = "HY Corporate Program")
               )

# plotly growth of lipper muni program portfolio
plot_ly() %>%
        add_trace(data = lipper_muni,
                  x = ~date,
                  y = ~portfolio_equity,
                  type = "scatter",
                  name = 'Portfolio Value',
                  mode = 'lines') %>% 
        layout(yaxis = list(title = "Value of Portfolio"),
               xaxis = list(title = "Date"),
               title = list(text = "HY Muni Program")
               )

# plotly growth of tactical program portfolio
plot_ly() %>%
        add_trace(data = tactical_program,
                  x = ~date,
                  y = ~tactical_portfolio_equity,
                  type = "scatter",
                  name = 'Portfolio Value',
                  mode = 'lines') %>% 
        layout(yaxis = list(title = "Value of Portfolio"),
               xaxis = list(title = "Date"),
               title = list(text = "Tactical Program")
        )
#### end plotly of growth of portfolios ####

############################ evaluating performance ############################
# calculate the annualized return of lipper hy program for the reporting period
lipper_hy %>% 
        filter(date > performance_from) %>%
        tq_transmute(select     = portfolio_return, 
                     mutate_fun = Return.annualized)

# calculate the annualized return of lipper muni program for the reporting period
lipper_muni %>% 
        filter(date > performance_from) %>%
        tq_transmute(select     = portfolio_return, 
                     mutate_fun = Return.annualized)

# calculate the annualized return of tactical program for the reporting period
tactical_program %>% 
        filter(date > performance_from) %>%
        tq_transmute(select     = tactical_portfolio_return, 
                     mutate_fun = Return.annualized)

# compare relative performance of hy vs hy muni
relative_hy <- lipper_hy %>% 
        filter(date > performance_from) %>% 
        select(date, portfolio_equity)
relative_muni <- lipper_muni %>% 
        filter(date > performance_from) %>% 
        select(date, portfolio_equity)
relative_hy_vs_muni <- left_join(relative_hy, relative_muni, by="date")
relative_hy_vs_muni <- relative_hy_vs_muni %>% 
        mutate(relative_performance = portfolio_equity.x/portfolio_equity.y)
plot_ly() %>%
        add_trace(data = relative_hy_vs_muni,
                  x = ~date,
                  y = ~relative_performance,
                  type = "scatter",
                  name = 'Relative Performance of HY vs Muni',
                  mode = 'lines')

# calculate the sharpe ratio of lipper hy program for the reporting period
lipper_hy %>% 
        filter(date > performance_from) %>%
        tq_performance(Ra = portfolio_return,
                       performance_fun = SharpeRatio)

# evaluate program returns with CAPM model
Ra <- lipper_hy %>% 
        filter(date > performance_from) %>% 
        tq_transmute(
                select = portfolio_equity,
                mutate_fun = periodReturn,
                period = "daily",
                col_rename = "Ra"
        )
Ra
Rb <- baseline %>%
        tq_get(get  = "stock.prices",
               from = performance_from,
               to   = performance_to) %>%
        tq_transmute(select     = adjusted, 
                     mutate_fun = periodReturn, 
                     period     = "daily", 
                     col_rename = "Rb")
Rb
RaRb <- left_join(Ra, Rb, by = c("date" = "date"))
RaRb
colSums(is.na(RaRb)) # check for missing data
capm_hy <- RaRb %>%
        tq_performance(Ra = Ra, 
                       Rb = Rb, 
                       performance_fun = table.CAPM)
capm_hy
