# load packages
library(quantmod)
library(PerformanceAnalytics)
library(tidyquant)
library(ggthemes)

# set range and end date of report
range <- '2003-04-01::2017-12-31'
end <- "2017-12-31"

# import historical data of FGM TACTICAL
fgm_tactical <- read.zoo(file = "data_inputs/fgm_tactical_ib.csv",
                            sep=",",
                            header = TRUE,
                            format="%m/%d/%y",
                            drop=FALSE,
                            )
fgm_tactical <- as.xts(fgm_tactical)
tail(fgm_tactical)

# create dataframe for plotting fgm_tactical
fgm_tactical_graph <- fgm_tactical
fgm_tactical_graph <- as.data.frame(date = index(fgm_tactical_graph),
                                    fgm_tactical_graph$Value,
                                )
fgm_tactical_graph <- fgm_tactical_graph %>%
        tibble::rownames_to_column()
fgm_tactical_graph$rowname <- as.Date(fgm_tactical_graph$rowname)
fgm_tactical_graph["symbol"] <- "TACTICAL INCOME BUILDER"
colnames(fgm_tactical_graph) <- c("date", "adjusted", "symbol")
head(fgm_tactical_graph)

# import Lipper High Yield Index from CSV
lipper_hy <- read.zoo(file = "data_inputs/lipper_hy.csv",
                      sep=",",
                      header = TRUE,
                      format="%m/%d/%y",
                      drop=FALSE,
                      )
lipper_hy <- as.xts(lipper_hy)
tail(lipper_hy)

# create dataframe for plotting lipper_hy
lipper_hy_graph <- lipper_hy
lipper_hy_graph <- as.data.frame(date = index(lipper_hy_graph),
                                 lipper_hy_graph$Value,
                                 )
lipper_hy_graph <- lipper_hy_graph %>%
        tibble::rownames_to_column()
lipper_hy_graph$rowname <- as.Date(lipper_hy_graph$rowname)
lipper_hy_graph["symbol"] <- "PEER GROUP BENCHMARK†"
colnames(lipper_hy_graph) <- c("date", "adjusted", "symbol")
head(lipper_hy_graph)

# import Barclays Bloomberg US Aggregate Bond Index from csv
# convert file to xts format
bbusab_zoo <- read.zoo(file = "data_inputs/bbusab_tr.csv",
                       sep=",",
                       header = TRUE,
                       format="%m/%d/%y", 
                       drop=FALSE,
                       )
bbusab_xts <- as.xts(bbusab_zoo)
bad <- is.na(bbusab_xts)
bbusab_tr <- bbusab_xts[!bad]
rm(bad)
rm(bbusab_xts)
rm(bbusab_zoo)
tail(bbusab_tr)

# create dataframe for plotting bbusab_tr
bbusab_tr_graph <- bbusab_tr
bbusab_tr_graph <- as.data.frame(date = index(bbusab_tr_graph),
                                 bbusab_tr_graph$Value,
                                 )
bbusab_tr_graph <- bbusab_tr_graph %>%
        tibble::rownames_to_column()
bbusab_tr_graph$rowname <- as.Date(bbusab_tr_graph$rowname)
bbusab_tr_graph["symbol"] <- "BENCHMARK 2†"
colnames(bbusab_tr_graph) <- c("date", "adjusted", "symbol")
head(bbusab_tr_graph)

# create plot
combined_graph <- rbind(fgm_tactical_graph, bbusab_tr_graph)
combined_graph <- rbind(lipper_hy_graph, combined_graph)
combined_graph %>%
        group_by(symbol) %>%
        filter(date >= "2003-03-31" & date <=end) %>%
        tq_transmute(select     = adjusted,
                     mutate_fun = periodReturn,
                     period     = "quarterly") %>%
        mutate(wealth.index = 100000 * cumprod(1 + quarterly.returns)) %>%
        ggplot(aes(x = date, y = wealth.index, color = symbol)) +
        geom_line(size = 1) +
        labs(title = NULL,
             subtitle = NULL,
             x = NULL, 
             y = NULL,
             color = NULL) +
        scale_y_continuous(labels = scales::dollar) +
        scale_x_date(date_breaks = "2 year",
                     date_labels = "%Y",
                     ) +
        theme_tq() +
        scale_colour_economist(breaks=c("TACTICAL INCOME BUILDER", "PEER GROUP BENCHMARK†", "BENCHMARK 2†")) +
        theme(axis.text.x = element_text(angle = 45, 
                                         hjust = 1,
                                         ),
              panel.border = element_blank(),
              panel.background = element_rect(fill = NULL,
                                              colour = NULL,
                                              size = 0.5, 
                                              linetype = "solid",
                                              ),
              plot.background = element_rect(fill = NULL,
                                             )
              )

# save plot as tiff file
ggsave("tactical_benchmarks.tiff",
       plot = last_plot(),
       device = "tiff",
       width = 7,
       height = 4.326,
       units = "in",
       dpi = 300,
       path = "../../../DesignWork/Clients/FGM/Projects/fact_sheets/_Inputs"
       )

# create and export csv of yearly & cumulative returns for FGM tatical
fgm_tactical_table <- fgm_tactical
fgm_tactical_table <- as.data.frame(date = index(fgm_tactical_table),
                                    fgm_tactical_table$Value,
                                )
fgm_tactical_table <- fgm_tactical_table %>%
        tibble::rownames_to_column()
fgm_tactical_table$rowname <- as.Date(fgm_tactical_table$rowname)
colnames(fgm_tactical_table) <- c("date", "Value")
fgm_tactical_yearly <- fgm_tactical_table %>%
        filter(date >= "2003-03-31" & date <=end) %>%
        tq_transmute(select     = Value,
                     mutate_fun = periodReturn,
                     period     = "yearly",) %>%
        mutate(cumulative.returns = cumprod(1 + yearly.returns)-1)
head(fgm_tactical_yearly)
write_csv(fgm_tactical_yearly, "../../../DesignWork/Clients/FGM/Projects/fact_sheets/_Inputs/tactical_cumulative.csv")

# create and export csv of quarterly returns for FGM tactical
fgm_tactical_table <- fgm_tactical
fgm_tactical_table <- as.data.frame(date = index(fgm_tactical_table),
                                    fgm_tactical_table$Value,
                                )
fgm_tactical_table <- fgm_tactical_table %>%
        tibble::rownames_to_column()
fgm_tactical_table$rowname <- as.Date(fgm_tactical_table$rowname)
colnames(fgm_tactical_table) <- c("date", "Value")
fgm_tactical_quarterly <- fgm_tactical_table %>%
        filter(date >= "2003-03-31" & date <=end) %>%
        tq_transmute(select     = Value,
                     mutate_fun = periodReturn,
                     period     = "quarterly",)
head(fgm_tactical_quarterly)
fgm_tactical_quarterly <- data.frame(matrix(fgm_tactical_quarterly$quarterly.returns, 
                                   ncol = 4, 
                                   byrow = TRUE))
colnames(fgm_tactical_quarterly) <- c("Q1", "Q2", "Q3", "Q4")
head(fgm_tactical_quarterly)
write_csv(fgm_tactical_quarterly, 
          "../../../DesignWork/Clients/FGM/Projects/fact_sheets/_Inputs/tactical_quarterly.csv")

# calcuate the quarterly returns of FGM tactical
fgm_tactical_quarterly <- periodReturn(fgm_tactical$Value,
                                   subset=range, 
                                   period='quarterly', 
                                   type='arithmetic')
tail(fgm_tactical_quarterly)

# calcuate the quarterly returns of BBUSAB
bbusab_quarterly <- periodReturn(bbusab_tr$Value, 
                                   subset=range, 
                                   period='quarterly', 
                                   type='arithmetic')
tail(bbusab_quarterly)

# calcuate the quarterly returns of Lipper Hy
lipperhy_quarterly <- periodReturn(lipper_hy$Value, 
                                   subset=range,
                                   period='quarterly', 
                                   type='arithmetic')
tail(lipperhy_quarterly)

# calcuate the annualized rate of return for FGM TACTICAL
fgm_tactical_annualized_rtn <- Return.annualized(fgm_tactical_quarterly)
fgm_tactical_annualized_rtn

# calcuate the annualized rate of return for Lipper HY since 3/31/2003
lipperhy_annualized_rtn <- Return.annualized(lipperhy_quarterly)
lipperhy_annualized_rtn

# calcuate the annualized rate of return for BBUSAB
bbusab_annualized_rtn <- Return.annualized(bbusab_quarterly)
bbusab_annualized_rtn

# combine annualized returns for export
row.names(fgm_tactical_annualized_rtn) <- "Financial Growth Management Tactical Income Builder"
row.names(lipperhy_annualized_rtn) <- "Peer Group Benchmark - Lipper High Yield Bond Index"
row.names(bbusab_annualized_rtn) <- "Benchmark 2 - Bloomberg Barclays U.S. Aggregate Bond Index"
annualized_returns <- rbind(fgm_tactical_annualized_rtn, lipperhy_annualized_rtn)
annualized_returns <- rbind(annualized_returns, bbusab_annualized_rtn)
colnames(annualized_returns) <- c("Annualized Returns")
annualized_returns
annualized_returns <- as.data.frame(annualized_returns)
annualized_returns <- annualized_returns %>%
        tibble::rownames_to_column()
write_csv(annualized_returns, 
          "../../../DesignWork/Clients/FGM/Projects/fact_sheets/_Inputs/tactical_annualized.csv")

# find the top 5 drawdowns of quarterly returns 2003-2017
a <- table.Drawdowns(fgm_tactical_quarterly,
                top = 5,
                digits = 4,
                geometric = TRUE)
b <- table.Drawdowns(lipperhy_quarterly,
                     top = 5,
                     digits = 4,
                     geometric = TRUE)
c <- table.Drawdowns(bbusab_quarterly,
                top = 5,
                digits = 4,
                geometric = TRUE)
d <- cbind(a$Depth, b$Depth)
combined_drawdowns <- cbind(d, c$Depth)
colnames(combined_drawdowns) <- c("Tactical Income Builder", "Peer Group Benchmark", "Benchmark 2")
combined_drawdowns <- as.data.frame(combined_drawdowns)
combined_drawdowns <- combined_drawdowns %>%
        tibble::rownames_to_column()
write_csv(combined_drawdowns, 
        "../../../DesignWork/Clients/FGM/Projects/fact_sheets/_Inputs/tactical_drawdowns.csv")
# now open tactical.xlsx and import csv files to update the report

