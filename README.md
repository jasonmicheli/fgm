# fgm

## sp_500.csv
Official Name: S&P 500® Index
https://us.spindices.com/indices/equity/sp-500

The launch date of the S&P 500 Index was March 4, 1957. It is a price return index.

The file sp_500.csv was originally sourced from Yahoo Finance, with data from to January 3, 1950 to July 17, 2018, and it includes daily high and low prices. Data beginning on January 1, 2018 is taken from the feed at the St. Louis Federal Reserve and has been combined with the date from Yahoo. A sampling of the "close" and "adjusted close" data has been confirmed against other sources; the annual returns calculated off of this file have also been confirmed against another source.

## bbusab_tr.csv
Official Name: Bloomberg Barclays US Aggregate Total Return Value Unhedged USD
https://data.bloomberglp.com/indices/sites/2/2016/08/Factsheet-US-Aggregate.pdf
https://www.bloomberg.com/quote/LBUSTRUU:IND
http://quotes.morningstar.com/indexquote/quote.html?t=XIUSA000MC&region=usa&culture=en-US

The Bloomberg Barclays US Aggregate Bond Index (ticker XIUSA000MC) was was created in 1986, with index history backfilled to January 1, 1976. The file bbusab_tr.csv tracks the total return of the index (ticker LBUSTRUU:IND), and was created by manually transferring data from Morningstar. The data is only month-end data from the initiation of the index through _________, after which time it is end of day data.

## fgm_hycb.csv
FGM High Yield Corporate Bond Program

The launch date of the HYCB Program was March, 31, 2003.

The data in fgm_hycb.csv is taken from a file prepared by Ed, and includes data only at the end of each quarter.

## lipper_muni_hy.csv
Contains month-end data from 1/31/03. Begins to have daily closing data as of 8/29/13.
